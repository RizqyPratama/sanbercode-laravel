<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <h1>Tugas 3</h1>
    <?php
    function tukar_besar_kecil($string)
    {
        $char = range('a', 'z');
        $kata = "";
        foreach (str_split($string) as $str) {
            if (ctype_lower($str)) {
                $kata .= strtoupper($str);
            } else {
                $kata .= strtolower($str);
            }
        }
        return $kata . "<br>";
    }


    // TEST CASES
    echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

    ?>

</body>

</html>